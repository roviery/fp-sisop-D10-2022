#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <arpa/inet.h>
#include <netinet/in.h>
#include <unistd.h>
#include <pthread.h>
#include <errno.h>
#include <dirent.h>
#include <errno.h>
#define PORT 8080

char root_path[1024] = "../databases";

int create_table_header(char *output, char data[100][100], char type[100][100], int numOfData)
{
    int totalSpace;
    char temp[60];
    char container[1024] = "";

    strcpy(output, "|");

    for (int i = 0; i < numOfData; i++)
    {
        memset(temp, 0, 60);
        sprintf(temp, "%s (%s)", data[i], type[i]);

        if(strcmp(type[i], "string") == 0)
        {
            totalSpace = 50 - strlen(temp);
            if (totalSpace < 0) return 11;
        }
        else if(strcmp(type[i], "char") == 0)  
        {
            totalSpace = 20 - strlen(temp);
            if (totalSpace < 0) return 12;
        }
        else if(strcmp(type[i], "int") == 0)  
        {
            totalSpace = 20 - strlen(temp);
            if (totalSpace < 0) return 13;

        }
        else return 14;

        char space[60] = "";
        for(int i = 0; i < totalSpace; i++)
        {
            strcat(space, " ");
        }

        sprintf(container, " %s%s |", temp, space);

        strcat(output, container);
    }
    return 1;
}
 
int create_table(char *table_path, char column_name[100][100], char type[100][100], int numOfCol)
{
    int checker;
    FILE *table = fopen(table_path, "w");

    char printed_row[1024];
    checker = create_table_header(printed_row, column_name, type, numOfCol);

    if(checker != 1) {
        fclose(table);
        return checker;
    }

    int length = strlen(printed_row);
    char logText[1024 * 4];
    char border[1024] = "";

    for(int i = 0; i < length; i++){
        strcat(border, "-");
    }

    sprintf(logText, "%s\n%s\n%s\n", border, printed_row, border);
    fputs(logText, table);

    fclose(table);

    return 1;
}

int insert_row(char *table_path, char data[100][60], int numOfCol)
{
    FILE *table;
    char err_msg[1024] = {0};
    char readLine[2084] = {0};
    char printedRow[1024];
    char container[70] = "";
    char *savePointer;
    int counter = 0;
    int dataCounter = 0;
    int length;
    
    if (table = fopen(table_path, "r")) // file exists
    {
        // Initialization
        strcpy(printedRow, "|");

        // Counting width
        while (fgets(readLine, sizeof(readLine), table)) 
        { 
            if (counter < 1) {
                counter++;
                continue;
            }
            char *token = strtok_r(readLine, "(", &savePointer);
            token = strtok_r(NULL, "(", &savePointer); 
            while (token != NULL)
            {
                if (token[0] == 's') // string
                {
                    length = 50;
                    if (strlen(data[dataCounter]) > length) 
                    {
                        fclose(table);
                        return 11;
                    }
                    
                    if(data[dataCounter][0] != '\'' || data[dataCounter][strlen(data[dataCounter]) - 1] != '\'') // alfabet tanpa tanda petik
                    {
                        fclose(table);
                        return 16;
                    }
                }
                else if (token[0] == 'c') // char
                {
                    length = 20;
                    if (strlen(data[dataCounter]) > length)
                    {
                        fclose(table);
                        return 12;
                    }

                    if(data[dataCounter][0] != '\'' || data[dataCounter][strlen(data[dataCounter]) - 1] != '\'') // alfabet tanpa tanda petik
                    {
                        fclose(table);
                        return 15;
                    }
                } 
                else if (token[0] == 'i') // int
                {
                    length = 20;
                    if (strlen(data[dataCounter]) > length)
                    {
                        fclose(table);
                        return 13;
                    }
                    for (int i = 0; i < strlen(data[dataCounter]); i ++)
                    {
                        if (data[dataCounter][i] < 48 || data[dataCounter][i] > 57) 
                        {
                            fclose(table);
                            return 14;
                        }
                    }
                }

                length -= strlen(data[dataCounter]);

                char space[60] = "";
                for(int k = 0; k < length; k++)
                {
                    strcat(space, " ");
                }

                sprintf(container, " %s%s |", data[dataCounter], space);
                strcat(printedRow, container);
                dataCounter++;
                token = strtok_r(NULL, "(", &savePointer); 
            }
            break;
        }

        // write row ke file
        fclose(table);
        table = fopen(table_path, "a");
        strcat(printedRow, "\n");
        fputs(printedRow, table);
        printf("%s\n", printedRow);
        fclose(table);
        return 1;
    }
    else // file does not exist
    {
        return 0;
    }

    return 0;
}

int insert_row_user(char *table_path, char data[100][60], int numOfCol)
{
    FILE *table;
    char err_msg[1024] = {0};
    char readLine[2084] = {0};
    char printedRow[1024];
    char container[70] = "";
    char *savePointer;
    int counter = 0;
    int dataCounter = 0;
    int length;
    
    if (table = fopen(table_path, "r")) // file exists
    {
        // Initialization
        strcpy(printedRow, "|");

        // Counting width
        while (fgets(readLine, sizeof(readLine), table)) 
        { 
            if (counter < 1) {
                counter++;
                continue;
            }
            char *token = strtok_r(readLine, "(", &savePointer);
            token = strtok_r(NULL, "(", &savePointer); 
            while (token != NULL)
            {
                length = 50;
                if (strlen(data[dataCounter]) > length) 
                {
                    fclose(table);
                    return 11;
                }

                length -= strlen(data[dataCounter]);

                char space[60] = "";
                for(int k = 0; k < length; k++)
                {
                    strcat(space, " ");
                }

                sprintf(container, " %s%s |", data[dataCounter], space);
                strcat(printedRow, container);
                dataCounter++;
                token = strtok_r(NULL, "(", &savePointer); 
            }
            break;
        }

        // write row ke file
        fclose(table);
        table = fopen(table_path, "a");
        strcat(printedRow, "\n");
        fputs(printedRow, table);
        printf("%s\n", printedRow);
        fclose(table);
        return 1;
    }
    else // file does not exist
    {
        return 0;
    }

    return 0;
}

int check_user(char *user)
{
    char path[1024];
    strcpy(path, root_path);
    strcat(path, "/users/user_list");
    
    FILE *table = fopen(path, "r");
    char line[1024]; 
    char *saveptr_user;
    int found = 0, counter = 0, tok_counter = 0;

    while (fgets(line, sizeof(line), table)) 
    {
        if (counter < 3)
        {
            counter++;
            continue;
        }

        char *token = strtok_r(line, " ", &saveptr_user);
        token = strtok_r(NULL, " ", &saveptr_user); 

        printf("token = %s\n", token);
        printf("user = %s\n", user);
        if (strcmp(token, user) == 0)
        {
            found = 1;
            break;
        }
    }

    fclose(table);

    if (found) return 1;

    return 0;
}

int check_database_user(char *database, char *user)
{
    char path[1024];
    strcpy(path, root_path);
    strcat(path, "/users/database_access");
    
    FILE *table = fopen(path, "r");
    char line[2048]; 
    char *saveptr_user;
    int found = 0, counter = 0, tok_counter = 0;

    if (check_user(user) == 0) return 3;

    while (fgets(line, sizeof(line), table)) 
    {
        if (counter < 3)
        {
            counter++;
            continue;
        }

        char *token = strtok_r(line, " ", &saveptr_user);
        token = strtok_r(NULL, " ", &saveptr_user); 

        if (strcmp(token, user) == 0)
        {
            token = strtok_r(NULL, " ", &saveptr_user); 
            token = strtok_r(NULL, " ", &saveptr_user); 
            if (strcmp(token, database) == 0)
            {
                found = 1;
                break;
            }
        }
    }

    fclose(table);

    return found;
}

void init_database()
{
    char path[1024];
    char col[100][100] = {0};
    char type[100][100] = {0};
    int col_counter;
    char err_msg[1024] = {0};

    DIR* dir = opendir(root_path);
    if (dir) 
        closedir(dir);
    else if (ENOENT == errno) 
    {
        int checker;
        mkdir(root_path, 0777);
        
        // Creating database users
        strcpy(path, root_path);
        strcat(path, "/users");
        mkdir(path, 0777);
        printf("Database 'users' has been created.\n");

        // Creating table 'user_list'
        strcat(path, "/user_list");
        // Initialization
        memset(col, 0, sizeof col);
        col_counter = 2;
        strcpy(type[0], "string");
        strcpy(type[1], "string");

        // Inserting column names
        strcpy(col[0], "Username");
        strcpy(col[1], "Password");
        // Creating the table
        checker = create_table(path, col, type, col_counter);
        printf("Table user_list has been created.\n");

        // Creating table 'database_access'
        strcpy(path, root_path);
        strcat(path, "/users");
        strcat(path, "/database_access");

        memset(col, 0, sizeof col);
        strcpy(col[0], "Username");
        strcpy(col[1], "Database");
        checker = create_table(path, col, type, col_counter);
        printf("Table database_access has been created.\n");

        // Creating history query directory
        stpcpy(path, root_path);
        strcat(path, "/history");
        mkdir(path, 0777);
        printf("Database 'history' has been created.\n");

    } 
}

int create_database(char *token)
{
    printf("Creating database...\n");
    char path[1024];
    int check;

    if (token == NULL) return 0;
    
    strcpy(path, root_path);
    strcat(path, "/");
    strcat(path, token);

    printf("%s\n", path);

    check = mkdir(path, 0777);

    if (check == -1){
        printf("Error Creating Database\n");
    }
    return check;
}

int create_user(char *token)
{
    char path[1024];
    strcpy(path, root_path);
    strcat(path, "/users/user_list");

    char username[100], password[100];
    char col[100][60] = {0};
    char *saveptr;
    int user_exist = 0;
    int col_counter = 2;
    token = strtok(NULL, " ");
    if (token == NULL) return 0;
    
    user_exist = check_user(token);
    if(user_exist == 0)
    {
        strcpy(username, token);

        token = strtok(NULL, " ");

        if (token == NULL) return 0;
        else 
        {
            if (strcmp(token, "IDENTIFIED") == 0)
            {
                
                token = strtok(NULL, " ");
                if (strcmp(token, "BY") == 0)
                {
                    token = strtok(NULL, " ");
                    if (token != NULL) 
                    {
                        strcpy(password, token);

                        // Initialization
                        strcpy(path, root_path);
                        strcat(path, "/users/user_list");

                        memset(col, 0, sizeof col);
                        
                        // Inserting row
                        int response;
                        strcpy(col[0], username);
                        strcpy(col[1], password);
                        printf("masuk sampe sini\n");
                        response = insert_row_user(path, col, col_counter);

                        if (response == 0)
                        {
                            return 2;
                            // strcpy(err_msg, "Unable to create new user (table doesn't exist).");      
                            
                        }
                        else
                        {
                            return response;
                            // strcpy(err_msg, "A new user has been created.");
                        }
                    }
                    else return 0;
                }
                else return 0;
            } 
            else return 0;
        }
    }
    else if(user_exist == 1)
    {
        return 3;
    }
}

int find_database(char *database, char *user)
{
    char path[1024];
    strcpy(path, root_path);
    strcat(path, "/");
    strcat(path, database);

    DIR* dir = opendir(path);
    if (dir) {
        closedir(dir);

        // Checking whether user can access this database
        int check;
        check = check_database_user(database, user);

        if (check == 1 || check == 3) return check; // 1 = user & database aman (ketemu & sesuai), 3 = user ngga ketemu
        else return 2;  // database ada, tapi ga ada permission
    }
    else if (ENOENT == errno) return 0;
    return 0; // database not exist
}

int find_table(char *path)
{
    FILE *table;
    if (table = fopen(path, "r"))
    {
        fclose(table);

        return 51;
    }
    else if (ENOENT == errno) return 50;
    return 50;
}

int drop_column (char *table_path, char *table_name, char *col_name)
{
    char line[2048];
    char *savePointer;
    char columns[100][100];
    char data_type[100][100];
    char data[1024][100][60];
    int total_col = 0;
    int col_loc = -1;
    FILE *table = fopen(table_path, "r");

    // Searching for column
    fgets(line, sizeof(line), table);
    fgets(line, sizeof(line), table);

    char *token = strtok_r(line, " ", &savePointer);
    int skip = 0;
    while(token != NULL)
    {
        //printf("token = %c\n", token[0]);
        if(token[0] == '|') 
        {
            token = strtok_r(NULL, " ", &savePointer);
            continue;
        }
        else if (strcmp(token, col_name) == 0) 
        {
            col_loc = total_col;
            skip = 1;
        }
        else if (token[0] == '(' && skip == 0)
        {
            if (token[1] == 's') strcpy(data_type[total_col], "string");
            else if (token[1] == 'c') strcpy(data_type[total_col], "char");
            else if (token[1] == 'i') strcpy(data_type[total_col], "int");
            printf("data type = %s\n", data_type[total_col]);

            total_col++;
        }
        else if (skip == 1) skip = 0;
        else
        {
            strcpy(columns[total_col], token);
            printf("columns = %s\n", columns[total_col]);
        }
        token = strtok_r(NULL, " ", &savePointer);

    }
    if (col_loc == -1) 
    {
        fclose(table);
        return 0; // column not found
    }

    fgets(line, sizeof(line), table);
    int checker = create_table(table_path, columns, data_type, total_col);
    int row_counter = 0;
    while (fgets(line, sizeof(line), table))
    {
        int data_counter = 0;
        int found = 0;
        token = strtok_r(line, " ", &savePointer);

        while(token != NULL)
        {
            if (strcmp(token, "|") == 0 || token[0] == '(') 
            {
                token = strtok_r(NULL, " ", &savePointer);
                continue;
            }
            else if (data_counter == col_loc && found == 0)
            {
                found = 1;
            }
            else 
            {
                strcpy(data[row_counter][data_counter], token);
                data_counter++;
            }
            token = strtok_r(NULL, " ", &savePointer);
        }
        row_counter++;
    }
    
    fclose(table);
    int insert_check;
    if (row_counter != 0)
    {
        for(int l = 0; l < row_counter; l++)
        {
            insert_check = insert_row(table_path, data[l], total_col);
        }
    }


    return 1;
}

void drop_table(char *table_path)
{
    char drop_table[1024];
    memset(drop_table, 0, sizeof drop_table);
    sprintf(drop_table, "%s %s", "rm", table_path);

    system(drop_table);
}

void print_log(char *username, char *command){
    FILE *log_file = fopen("./log.txt", "a");
    char logText[1000];
    time_t t = time(NULL);
    struct tm tm = *localtime(&t);
    if (strcmp(username, "@") == 0){
        sprintf(logText, "%d-%02d-%02d %02d:%02d:%02d:root:%s\n", tm.tm_year+1900, tm.tm_mon+1, tm.tm_mday, tm.tm_hour, tm.tm_min, tm.tm_sec, command);
    }
    else sprintf(logText, "%d-%02d-%02d %02d:%02d:%02d:%s:%s\n", tm.tm_year+1900, tm.tm_mon+1, tm.tm_mday, tm.tm_hour, tm.tm_min, tm.tm_sec, username, command);
    fputs(logText, log_file);

    fclose(log_file);
}

void parse_command(char *command, char *use_database){
    FILE *history_file;
    char path[1024];
    char text[1024];

    strcpy(path, root_path);
    strcat(path, "/history/");
    strcat(path, use_database);
    history_file = fopen(path, "a");

    char *token;
    token = strtok(command, ";");

    while (token != NULL){
        memset(text, 0, sizeof text);
        if (token[0] == ' '){
            memmove(token, token+1, strlen(token));
        }
        printf("%s;\n", token);
        sprintf(text, "%s;\n", token);
        fputs(text, history_file);
        token = strtok(NULL, ";");
    }

    fclose(history_file);
}

int count_column(char *table_path)
{    
    FILE *table = fopen(table_path, "r");
    char line[2048]; 
    char *saveptr, *iterator;
    int counter = 0, tok_counter = 0;

    fgets(line, sizeof(line), table);
    fgets(line, sizeof(line), table);

    iterator = strtok_r(line, " ", &saveptr);
    while (iterator != NULL)
    {
        if(iterator[0] == '|' || iterator[0] == '(') 
        {
            iterator = strtok_r(NULL, " ", &saveptr);
        }
        else 
        {
            counter++;
            iterator = strtok_r(NULL, " ", &saveptr);
        }
    }

    return counter;
}

int find_col(char *table_path, char *col_name)
{
    char line[2048];
    char *savePointer;
    char columns[100][100];
    char data_type[100][100];
    char data[1024][100][60];
    int total_col = 0;
    int col_loc = -1;
    FILE *table = fopen(table_path, "r");

    // Searching for column
    fgets(line, sizeof(line), table);
    fgets(line, sizeof(line), table);

    char *token = strtok_r(line, " ", &savePointer);
    while(token != NULL)
    {
        //printf("token = %c\n", token[0]);
        if(token[0] == '|') 
        {
            token = strtok_r(NULL, " ", &savePointer);
            continue;
        }
        else if (strcmp(token, col_name) == 0) 
        {
            col_loc = total_col + 1;
            break;
        }
        else if (token[0] == '(')
        {
            total_col++;
        }

        token = strtok_r(NULL, " ", &savePointer);
    }

    if (col_loc == -1) 
    {
        fclose(table);
        return 0; // column not found
    }
    else return col_loc;
}

int update(char *table_path, char *newValue, int colNum)
{
    FILE *table;
    int length;
    char *savePtr;
    char err_msg[1024] = {0};
    char readLine[2084] = {0};

    table = fopen(table_path, "r");
    
    fgets(readLine, sizeof(readLine), table);
    fgets(readLine, sizeof(readLine), table);

    char *token = strtok_r(readLine, "(", &savePtr);

    for(int i = 0; i < colNum; i++)
    {
        token = strtok_r(readLine, "(", &savePtr);
    }

    if(token[0] == 's')
    {
        length = 50;
        if (strlen(newValue) > length) 
        {
            fclose(table);
            return 11;
        }
        
        if(newValue[0] != '\'' || newValue[strlen(newValue) - 1] != '\'') // alfabet tanpa tanda petik
        {
            fclose(table);
            return 16;
        }
    }
    else if(token[0] == 'c')
    {
        length = 20;
        if (strlen(newValue) > length) 
        {
            fclose(table);
            return 12;
        }
        
        if(newValue[0] != '\'' || newValue[strlen(newValue) - 1] != '\'') // alfabet tanpa tanda petik
        {
            fclose(table);
            return 15;
        }
    }
    else if (token[0] == 'i') // int
    {
        length = 20;
        if (strlen(newValue) > length)
        {
            fclose(table);
            return 13;
        }
        for (int i = 0; i < strlen(newValue); i++)
        {
            if (newValue[i] < 48 || newValue[i] > 57) 
            {
                fclose(table);
                return 14;
            }
        }
    }

    fgets(readLine, sizeof(readLine), table);

    while(fgets(readLine, sizeof(readLine), table))
    {
        char *token = strtok_r(readLine, " ", &savePtr);
        int counter = 0;
        while(token != NULL)
        {
            //printf("token = %c\n", token[0]);
            if(token[0] != '|') 
            {
                counter++;
            }

            if(counter == colNum)
            {
                
            }

            token = strtok_r(NULL, " ", &savePtr);
        }
    }

}

int main(int argc, char const *argv[]) 
{
    int server_fd, new_socket, valread;
    struct sockaddr_in address;
    int opt = 1;
    int addrlen = sizeof(address);
    char buffer[1024] = {0};
    char username[60];
    char use_database[100] = "";
      
    if ((server_fd = socket(AF_INET, SOCK_STREAM, 0)) == 0) 
    {
        perror("socket failed");
        exit(EXIT_FAILURE);
    }
      
    if (setsockopt(server_fd, SOL_SOCKET, SO_REUSEADDR | SO_REUSEPORT, &opt, sizeof(opt))) 
    {
        perror("setsockopt");
        exit(EXIT_FAILURE);
    }

    address.sin_family = AF_INET;
    address.sin_addr.s_addr = INADDR_ANY;
    address.sin_port = htons( PORT );
      
    if (bind(server_fd, (struct sockaddr *)&address, sizeof(address))<0) 
    {
        perror("bind failed");
        exit(EXIT_FAILURE);
    }

    if (listen(server_fd, 3) < 0) 
    {
        perror("listen");
        exit(EXIT_FAILURE);
    }

    if ((new_socket = accept(server_fd, (struct sockaddr *)&address, (socklen_t*)&addrlen))<0) 
    {
        perror("accept");
        exit(EXIT_FAILURE);
    }

    // Checking whether client is sudo
    int sudo = 0;
    char sudo_checker[10] = {0};
    read(new_socket, sudo_checker, 1024);
    if(sudo_checker[0] == 's') sudo = 1;

    // Making directory /databases to contain all the database
    init_database();

    // Receiving username
    if (sudo == 0) {
        read(new_socket, username, 60);
        if(strcmp(username, "Log in failed") == 0) 
        {
            printf("%s.\n", username);
            exit(1);
        }
        else printf("Username %s has logged in.\n", username);
    }
    else printf("Root has logged in.\n");

    // Checking for commands
    while(1){
        char msg[1024];
        memset(buffer, 0, 1024);
        valread = read(new_socket, buffer, 1024);
        char *command = buffer;

        if (command != NULL){
            if (strlen(use_database) != 0){
                parse_command(command, use_database);
            }
            print_log(username, command);
        }
        
        char *token = strtok(command, " ");

        if (token == NULL) {
            strcpy(msg, "Invalid Command.");
            send(new_socket, msg, strlen(msg), 0);
            continue;
        }
 
        // Authorization
        if (strcmp(token, "GRANT") == 0)
        {
            if(sudo == 0)
            {
                strcpy(msg, "Root access is needed to be able to grant permission.");
                send(new_socket, msg, strlen(msg), 0);
                continue;
            }
            char tok_database[100];
            char tok_user[100];
            token = strtok(NULL, " ");
            if (token == NULL) 
            {
                strcpy(msg, "Unable to grant permission.");
                send(new_socket, msg, strlen(msg), 0);
                continue;
            }
            else if (strcmp(token, "PERMISSION") == 0)
            {
                token = strtok(NULL, " ");
                if (token != NULL)
                {
                    strcpy(tok_database, token);
                    token = strtok(NULL, " ");
                    if (token != NULL && strcmp(token, "INTO") == 0)
                    {
                        token = strtok(NULL, " ");
                        if (token != NULL)
                        {
                            strcpy(tok_user, token);
                            int check;

                            check = find_database(tok_database, tok_user);

                            if (check == 2)
                            {
                                // masukkin row baru ke database_access
                                char path[1024];
                                char col[100][60] = {0};
                                strcpy(path, root_path);
                                strcat(path, "/users/database_access");

                                strcpy(col[0], tok_user);
                                strcpy(col[1], tok_database);

                                if(insert_row(path, col, 2) == 1) strcpy(msg, "Successfully granted a new access to the specified user.");
                                else strcpy(msg, "Unable to grant permission.");
                            }
                            else if (check == 1) strcpy(msg, "The user already has access to the database.");
                            else if (check == 3) strcpy(msg, "Specified user not exist.");
                            else if (check == 0) strcpy(msg, "Unable to find database.");
                        }
                        else strcpy(msg, "Unable to grant permission.");
                    }
                    else strcpy(msg, "Unable to grant permission.");
                }
                else strcpy(msg, "Unable to grant permission.");
            }
            else strcpy(msg, "Unable to grant permission.");

            send(new_socket, msg, strlen(msg), 0);
            continue;
        }
        else if (strcmp(token, "USE") == 0)
        {
            FILE *history_file;
            char path[1024];


            token = strtok(NULL, " ");
            if (token == NULL) 
            {
                strcpy(msg, "Unable to create.");
            }
            else if(sudo == 1) 
            {
                sprintf(msg, "Database %s is now in use.", token);
                strcpy(use_database, token);

                strcpy(path, root_path);
                strcat(path, "/history/");
                strcat(path, use_database);
                history_file = fopen(path, "a");
                fclose(history_file);
            }
            else 
            {
                int checker;
                checker = find_database(token, username);

                if(checker == 1 || sudo == 1) 
                {
                    sprintf(msg, "Database %s is now in use.", token);
                    strcpy(use_database, token);
                }
                else if(checker == 2)
                {
                    sprintf(msg, "User %s cannot access this database.", username);
                } 
                else strcpy(msg, "Database not found.");
            }
            send(new_socket, msg, strlen(msg), 0);
            continue;
        }
        // DDL
        else if (strcmp(token, "CREATE") == 0)
        {
            printf("------CREATE-----\n");

            token = strtok(NULL, " ");
            if (token == NULL) 
            {
                strcpy(msg, "Unable to create.");
                send(new_socket, msg, strlen(msg), 0);
                continue;
            }
            else if (strcmp(token, "USER") == 0)
            {
                if(sudo)
                {
                    int check;
                    check = create_user(token);

                    if (check == 1) strcpy(msg, "A new user has been created.");
                    else if (check == 2) strcpy(msg, "Unable to create new user (table doesn't exist).");
                    else if (check == 3) strcpy(msg, "User already exist.");
                    else if(check == 0) strcpy(msg, "Unable to create a new user.");
                    else if(check == 11) strcpy(msg, "String data length can not be more than 50 characters.");
                    else if(check == 12) strcpy(msg, "Char data length can not be more than 20 characters.");
                    else if(check == 13) strcpy(msg, "Int data length can not be more than 20 characters.");
                    else if(check == 14) strcpy(msg, "Int data type can not accept non numerical letter");
                }
                else
                {
                    strcpy(msg, "Root access is required to create a new user.");
                }

                send(new_socket, msg, strlen(msg), 0);
                continue;
            }
            else if (strcmp(token, "DATABASE") == 0){
                token = strtok(NULL, " ");
                if (token == NULL)
                {
                    strcpy(msg, "Unable to create database.");
                    send(new_socket, msg, strlen(msg), 0);
                    continue;
                }

                int check;
                check = find_database(token, username);

                if(check == 1 || check == 2) strcpy(msg, "Database already exist.");
                else
                {
                    check = create_database(token);
                    if(check == -1 && check == 0)
                    {
                        strcpy(msg, "Unable to create database.");
                    }
                    else 
                    {
                        if(sudo == 0)
                        {
                            // masukkin row baru ke database_access
                            char path[1024];
                            char col[100][60] = {0};
                            int insert;
                            strcpy(path, root_path);
                            strcat(path, "/users/database_access");

                            
                            strcpy(col[0], username);
                            strcpy(col[1], token);

                            insert = insert_row(path, col, 2);

                            if(insert == 1) strcpy(msg, "Database has been created.");
                            else if (insert == 11) strcpy(msg, "Name of database can not be more than 50 characters.");;
                        }
                        else strcpy(msg, "Database has been created.");
                    }
                }
                send(new_socket, msg, strlen(msg), 0);
                continue;
            }
            else if (strcmp(token, "TABLE") == 0){
                int checker;
                char column_name[100][100];
                char data_type[100][100];
                int counter = 0;
                token = strtok(NULL, " "); // -> table1
                if (token == NULL) {
                    strcpy(msg, "Unable to create table.");
                    send(new_socket, msg, strlen(msg), 0);
                    continue;
                }
                printf("used database %s\n", use_database);
                if (strcmp(use_database, "") == 0) {
                    strcpy(msg, "No database is used.");
                    send(new_socket, msg, strlen(msg), 0);
                    continue;
                }

                char table_path[1024] = "../databases/";
                strcat(table_path, use_database); // -> $use_database mengikuti authorisasi
                strcat(table_path, "/");
                strcat(table_path, token);

                // mengecek apakah tabel sudah ada atau belum
                checker = find_table(table_path);
                if (checker == 51)
                {
                    strcpy(msg, "Table already exist.");
                    send(new_socket, msg, strlen(msg), 0);
                    continue;
                }

                token = strtok(NULL, "("); // -> kolom1 string, kolom2 int)
                if (token == NULL) {
                    strcpy(msg, "Unable to create table.");
                    send(new_socket, msg, strlen(msg), 0);
                    continue;
                }

                char table_details[1024];
                strcpy(table_details, token);

                token = strtok(table_details, " "); // -> kolom1
                if (token == NULL) {
                    strcpy(msg, "Unable to create table.");
                    send(new_socket, msg, strlen(msg), 0);
                    continue;
                }

                while(token != NULL){
                    strcpy(column_name[counter], token);

                    token = strtok(NULL, " "); // -> string -> int
                    if (token == NULL) 
                        break;

                    
                    strcpy(data_type[counter], token);

                    if (data_type[counter][strlen(data_type[counter])-1] == ')' || data_type[counter][strlen(data_type[counter])-1] == ',')
                        data_type[counter][strlen(data_type[counter])-1] = '\0';

                    printf("nama kolom = %s\n", column_name[counter]);
                    printf("tipe data = %s\n", data_type[counter]);

                    counter++;
                    token = strtok(NULL, " "); // -> kolom2 -> NULL
                }
                checker = create_table(table_path, column_name, data_type, counter);
                if (checker == 1) strcpy(msg, "Table has been created.");
                else if(checker == 11) strcpy(msg, "String column name can not be more than 41 characters.");
                else if(checker == 12) strcpy(msg, "Char column name can not be more than 13 characters.");
                else if(checker == 13) strcpy(msg, "Int column name can not be more than 14 characters.");
                else if(checker == 14) strcpy(msg, "Invalid data type.");

                send(new_socket, msg, strlen(msg), 0);
                continue;
            }    
            else 
            {
                strcpy(msg, "Unable to create.");
                send(new_socket, msg, strlen(msg), 0);
                continue;
            }   
        }
        else if (strcmp(token, "DROP") == 0)
        {
            printf("------drop-----\n");

            token = strtok(NULL, " ");
            if (token == NULL)
            {
                strcpy(msg, "Unable to perform drop command.");
                send(new_socket, msg, strlen(msg), 0);
                continue;
            }

            if (strcmp(token, "DATABASE") == 0)
            {
                printf("-----database-----\n");

                int checker;
                int access = 0;

                token = strtok(NULL, " ");
                if (token == NULL) {
                    strcpy(msg, "Unable to drop database.");
                    send(new_socket, msg, strlen(msg), 0);
                    continue;
                }

                // Checking access & checking whether database exist
                if (sudo) access = 1;
                else {
                    checker = find_database(token, username);
                    access = checker;
                }

                if (access == 1)
                {
                    char database_path[200];
                    strcpy(database_path, root_path);
                    strcat(database_path, "/");
                    strcat(database_path, token);

                    char drop_database[1024];
                    memset(drop_database, 0, sizeof drop_database);
                    sprintf(drop_database, "%s %s", "rm -rf", database_path);

                    system(drop_database);

                    if(strcmp(use_database, token) == 0) strcpy(use_database, "");

                    strcpy(msg, "Successfully dropped database.");
                    send(new_socket, msg, strlen(msg), 0);
                    continue;
                }
                else if (access = 2){
                    sprintf(msg, "User %s unable to drop this database.", username);
                    send(new_socket, msg, strlen(msg), 0);
                    continue;
                }
                else if (access = 0){
                    strcpy(msg, "Database not exist.");
                    send(new_socket, msg, strlen(msg), 0);
                    continue;
                }
            }
            else if (strcmp(token, "TABLE") == 0)
            {
                printf("-----table-----\n");
                int checker = 0;

                token = strtok(NULL, " ");
                if (token == NULL) 
                {
                    strcpy(msg, "Unable to drop database.");
                    send(new_socket, msg, strlen(msg), 0);
                    continue;
                }
                if(strcmp(use_database, "") == 0) 
                {
                    strcpy(msg, "No database is used.");
                    send(new_socket, msg, strlen(msg), 0);
                    continue;
                }

                char table_path[200];
                strcpy(table_path, root_path);
                strcat(table_path, "/");
                strcat(table_path, use_database);
                strcat(table_path, "/");
                strcat(table_path, token);

                checker = find_table(table_path);
                if (checker == 51)
                {
                    drop_table(table_path);

                    strcpy(msg, "Successfully dropped the table.");
                    send(new_socket, msg, strlen(msg), 0);
                    continue;
                }
                else
                {
                    strcpy(msg, "No table found.");
                    send(new_socket, msg, strlen(msg), 0);
                    continue;
                }
            }   
            else if (strcmp(token, "COLUMN") == 0)
            {
                int checker;
                char target_col[60];
                token = strtok(NULL, " ");
                if (token == NULL)
                {
                    strcpy(msg, "Unable to drop column.");
                    send(new_socket, msg, strlen(msg), 0);
                    continue;
                }

                strcpy(target_col, token);
                token = strtok(NULL, " ");

                if (token == NULL || strcmp(token, "FROM") != 0)
                {
                    strcpy(msg, "Unable to drop column.");
                    send(new_socket, msg, strlen(msg), 0);
                    continue;
                }

                token = strtok(NULL, " ");
                char table_path[200];
                strcpy(table_path, root_path);
                strcat(table_path, "/");
                strcat(table_path, use_database);
                strcat(table_path, "/");
                strcat(table_path, token);
                // Checking the existance of the table
                checker = find_table(table_path);
                if (checker == 51) // table found
                {
                    checker = drop_column(table_path, token, target_col);
                    if (checker == 0) strcpy(msg, "No column found.");
                    else if (checker == 1) strcpy(msg, "Column successfully deleted.");
                    send(new_socket, msg, strlen(msg), 0);

                }
                else
                {
                    strcpy(msg, "No table found.");
                    send(new_socket, msg, strlen(msg), 0);
                    continue;
                }
            }
        }
        // DML
        else if (strcmp(token, "INSERT") == 0)
        {
            token = strtok(NULL, " ");
            if (token == NULL) 
            {
                strcpy(msg, "Unable to insert column.");
                send(new_socket, msg, strlen(msg), 0);
                continue;
            }
            else if (strcmp(token, "INTO") == 0)
            {
                token = strtok(NULL, " ");
                if (token == NULL) 
                {
                    strcpy(msg, "Unable to insert column.");
                    send(new_socket, msg, strlen(msg), 0);
                    continue;
                }
                else 
                {
                    char table_path[200];
                    strcpy(table_path, root_path);
                    strcat(table_path, "/");
                    strcat(table_path, use_database);
                    strcat(table_path, "/");
                    strcat(table_path, token);

                    // Checking whether table exist in the database or not
                    int checker;
                    checker = find_table(table_path);
                    if (checker == 51)
                    {
                        // counting column in the specified table
                        int numOfCol;
                        numOfCol = count_column(table_path);

                        // parse values
                        if (token == NULL) 
                        {
                            strcpy(msg, "Unable to insert column.");
                            send(new_socket, msg, strlen(msg), 0);
                            continue;
                        }

                        char data[100][60];
                        memset(data, 0, sizeof data);
                        for(int i = 0; i < numOfCol; i++)
                        {
                            int dataChar = 0;
                            token = strtok(NULL, ",");
                            printf("token ins , = %s\n", token);
                            if(i != 0 && token == NULL) // kekurangan data
                            {
                                strcpy(msg, "Insufficient argument. Unable to perform insert.");
                                send(new_socket, msg, strlen(msg), 0);
                                continue;
                            }
                            int len = strlen(token);
                            for(int j = 0; j < len; j++)
                            {
                                printf(" uhu = %c\n", token[j]);
                                if( j == 0 && ( token[j] == '(' || token[j] == ' ' ) ||
                                    j == len - 1 && token[j] == ')')
                                {
                                    continue;
                                }
                                else if((token[j] < 48 || token[j] > 57) && j != 0 && j != len-1 &&
                                        ( !(token[1] == '\'' && token[len - 1] == '\'') && 
                                         !(token[1] == '\'' && token[len - 2] == '\'' && token[len - 1] == ')'))) // alfabet tanpa tanda petik
                                {
                                    printf("oh = %c\n", token[1]);
                                    printf("oh = %c\n", token[len - 1]);
                                    printf("oh = %c\n", token[len - 2]);

                                    strcpy(msg, "String or char data type need ' in between.");
                                    send(new_socket, msg, strlen(msg), 0);
                                    continue;
                                }
                                else 
                                {
                                    data[i][dataChar] = token[j];
                                    dataChar++;
                                }
                            }
                            printf("%s\n", data[i]);
                        }
                        token = strtok(NULL, ",");
                        if (token != NULL) // kelebihan data
                        {
                            strcpy(msg, "Excessive argument. Unable to perform insert.");
                            send(new_socket, msg, strlen(msg), 0);
                            continue;
                        }

                        int check = insert_row(table_path, data, numOfCol);

                        if (check == 1) strcpy(msg, "A new record has been inserted.");
                        else if(check == 11) strcpy(msg, "String data length can not be more than 50 characters.");
                        else if(check == 12) strcpy(msg, "Char data length can not be more than 20 characters.");
                        else if(check == 13) strcpy(msg, "Int data length can not be more than 20 characters.");
                        else if(check == 14) strcpy(msg, "Int data type can not accept non numerical letter.");
                        else if(check == 15) strcpy(msg, "Input of char data type need ' in between.");
                        else if(check == 16) strcpy(msg, "Input of string data type need ' in between.");

                        send(new_socket, msg, strlen(msg), 0);
                        continue;
                            
                    }
                    else
                    {
                        strcpy(msg, "No table found.");
                        send(new_socket, msg, strlen(msg), 0);
                        continue;
                    }

                }
            }
        }
        else if (strcmp(token, "UPDATE") == 0)
        {
            token = strtok(NULL, " ");
            if (token == NULL) 
            {
                strcpy(msg, "Unable to update table.");
                send(new_socket, msg, strlen(msg), 0);
                continue;
            }

            char table_path[200];
            strcpy(table_path, root_path);
            strcat(table_path, "/");
            strcat(table_path, use_database);
            strcat(table_path, "/");
            strcat(table_path, token);
            int table_check = find_table(token);

            if (table_check == 51)
            {
                token = strtok(NULL, " ");
                if (token == NULL || strcmp(token, "SET") != 0) 
                {
                    strcpy(msg, "Unable to update table.");
                    send(new_socket, msg, strlen(msg), 0);
                    continue;
                }

                token = strtok(NULL, " ");
                if (token == NULL) 
                {
                    strcpy(msg, "Unable to update table.");
                    send(new_socket, msg, strlen(msg), 0);
                    continue;
                }

                // find kolom
                char col_equal[60], value [60];
                memset(col_equal, 0, sizeof col_equal);
                token = strtok(NULL, "=");

                int kolom_check = find_col(table_path, token);
                if (kolom_check == 0)
                {
                    strcpy(msg, "No column found.");
                    send(new_socket, msg, strlen(msg), 0);
                    continue;
                }

                strcpy(col_equal, token);

                // value
                token = strtok(NULL, "=");
                if (token == NULL) 
                {
                    strcpy(msg, "Unable to update table.");
                    send(new_socket, msg, strlen(msg), 0);
                    continue;
                }
                strcpy(value, token);

                // where
                token = strtok(NULL, " ");
                if (token != NULL) 
                {
                    if (strcmp(token, "WHERE") == 0)
                    {
                        // pake WHERE
                    }
                    else
                    {
                        strcpy(msg, "Invalid format.");
                        send(new_socket, msg, strlen(msg), 0);
                        continue;
                    }
                }
                else 
                {
                    // ga pake WHERE

                }


            }
            else if (table_check == 50)
            {
                strcpy(msg, "No table found.");
                send(new_socket, msg, strlen(msg), 0);
                continue;
            }
        }
        
        memset(buffer, 0, 1024);
    }
}
